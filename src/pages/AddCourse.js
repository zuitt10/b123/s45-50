import React,{useContext,useState,useEffect} from 'react';
import {Form,Button} from 'react-bootstrap';
import Banner from '../components/Banner';
import UserContext from '../userContext';

import Swal from 'sweetalert2';

import {Redirect,useHistory} from 'react-router-dom';


export default function AddCourse(){

	const{user} = useContext(UserContext);
	console.log(user);

	const history = useHistory();

	const [courseName,setCourseName] = useState("");
	const [description,setDescription] = useState("");
	const [price,setPrice] = useState("");
	// conditional rendering for button
	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{

		// console.log(courseName);
		// console.log(description);
		// console.log(price);

		if(courseName !== "" && description !== "" && price !==""){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	},[courseName,description,price])

	function createCourse(e){

		e.preventDefault()

		// console.log(courseName);
		// console.log(description);
		// console.log(price);
		// console.log(localStorage.getItem('token'))

		fetch('http://localhost:4000/courses/',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: courseName,
				description: description,
				price: price
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data.name){

				Swal.fire({
				icon: "success",
					title: "Course Added Successfully!",
					text: `New course ${data.name} has been added successfully`
				})


				history.push('/courses')
				


			} else {

				Swal.fire({

					icon: "error",
					title: "Add Course Failed.",
					text: data.message

				})
				setCourseName("");
				setDescription("");
				setPrice("");

			}

		})

	}

	let notFoundProp = {
			title: "The page you are trying to access is unavailable",
			description: "View our courses.",
			buttonCallToAction: "Back to home page",
			destination: "/"
		};

	return (
		user.isAdmin !== true
		?
		//<Redirect to="/"/>
		<Banner bannerProp={notFoundProp}/>
		:
		<>
			<h1 className="my-5 text-center">Add Course</h1>
			<Form onSubmit={e => createCourse(e)}>
				<Form.Group>
					<Form.Label>Course Name:</Form.Label>
					<Form.Control type="text" value={courseName} onChange={e => {setCourseName(e.target.value)}} placeholder="Enter Course Name" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Description:</Form.Label>
					<Form.Control type="text" value={description} onChange={e => {setDescription(e.target.value)}} placeholder="Enter Description" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Price (PHP):</Form.Label>
					<Form.Control type="number" value={price} onChange={e => {setPrice(e.target.value)}} placeholder="Enter Price" required/>
				</Form.Group>
				{
					isActive
					? <Button variant="primary" type="submit">Submit</Button>
					:	<Button variant="danger" disabled>Submit</Button>
				}
			</Form>
		</>
		)

}
