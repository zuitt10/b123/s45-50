import React,{useState,useEffect,useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
// import Swal
import Swal from 'sweetalert2';

// import user context
import UserContext from '../userContext'

// import Redirect component, this will allow us to redirect our user to one of our pages.
import {Redirect} from 'react-router-dom'

export default function Login() {

	const {user,setUser} = useContext(UserContext);
	/*console.log(user)*/
	
		// state for email and dpassword
		const [email,setEmail] = useState("");
		const [password,setPassword] = useState("");

		// state for conditionally rendering our button
		const [isActive,setIsActive] = useState(false)

		useEffect(() =>{

			if(email !=="" && password !== ""){

			setIsActive(true)
		} else {

			setIsActive(false)
		}

		},[email,password])

function loginUser(e){

	e.preventDefault();

				fetch('http://localhost:4000/users/login',{

					method: 'POST',
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						email:email,
						password:password
					})
				})
				.then(res => res.json())
				.then(data =>{
					console.log(data)
					if(data.accessToken){
						Swal.fire({
					icon: "success",
					title: "Login Successfull",
					text: `Thank you for Logging in`

				})
					localStorage.setItem('token',data.accessToken)
					fetch('http://localhost:4000/users/getUserDetails',{
		headers:{
			'Authorization': `Bearer ${data.accessToken}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		setUser({

			id: data._id,
			isAdmin: data.isAdmin
		})
	})
					} else {
						Swal.fire({
							icon: "error",
					title: "Login Failed",
					text: data.message
						})
					}
				})
		}
			
		return (
			user.id
			?
			<Redirect to ="/courses"/>
			:
			<>
				<h1 className="my-5 text-center">Login</h1>
			<Form onSubmit ={e => loginUser(e)}>
					<Form.Group>
						<Form.Label>Email:</Form.Label>
						<Form.Control 
						type="email" 
						value={email} 
						onChange={e =>{setEmail(e.target.value)}} 
						placeholder="Enter Email" 
						required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password:</Form.Label>
						<Form.Control 
						type="password" 
						value={password} 
						onChange={e =>{setPassword(e.target.value)}} 
						placeholder="Enter Password" 
						required/>
					</Form.Group>
					{
						isActive
						? <Button variant="primary" type="submit">Submit</Button>
						: <Button variant="danger" type="submit" disabled>Submit</Button>
					}
					
			</Form>

			</>
			
			   )

}