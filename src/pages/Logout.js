import React,{useContext,useEffect} from 'react';
import UserContext from '../userContext';
import Banner from '../components/Banner';

export default function Logout(){


const {setUser,unsetUser} = useContext(UserContext);
/*console.log(setUser)
console.log(unsetUser)*/

	// clear the localStorage 
	unsetUser();
	// add empty dependecy array to run the useEffect only on initial render.
	useEffect(() =>{
		// set the global user state to its initial value.
		// Can you  update a state included in the context with its setter function? Yes.
		setUser({
			id:null,
			isAdmin: null
		})
	},[])

	const bannerContent = {
		title: "See You later!",
		description: "You have logged out of J-Store Booking System",
		buttonCallToAction: "Go Back To Home Page.",
		destination: "/"
	}

	return (

			<Banner bannerProp={bannerContent} />
		)
}