import React from 'react';
import {Row,Col,Card} from 'react-bootstrap';

export default function Highlights() {

	return (

			<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum sint voluptate et commodo in in do irure laboris eiusmod cupidatat commodo quis aute voluptate.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

				<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Elit sunt adipisicing veniam labore excepteur deserunt amet dolor dolor pariatur minim ad consectetur nulla esse reprehenderit incididunt dolor in ex aliquip dolor ullamco do consequat sed dolore pariatur nulla dolor.
						</Card.Text>
					</Card.Body>
					</Card>
				</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlights">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of Our Community</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum sint voluptate et commodo in in do irure laboris eiusmod cupidatat commodo quis aute voluptate.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>


		)

}