// When Creating a component in a seperate file, we import:
// require() and import both imports your modules.
import React,{useState,useEffect} from 'react';
import Banner from './components/Banner';
import AppNavBar from './components/AppNavBar';
import Course from './components/Course';
import Highlights from './components/Highlights';
import NotFound from './components/NotFound';


// import react router dom components for simulated page router
import {BrowserRouter as Router} from 'react-router-dom';
import {Route,Switch} from 'react-router-dom';

// import our user provider - provider component of our context
import {UserProvider} from './userContext';

// import pages
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import AddCourse from './pages/AddCourse'
import ViewCourse from './pages/ViewCourse'

// import container from react-bootstrap
import{Container} from 'react-bootstrap';


// import app css in this component
import './App.css';

export default function App() {



const [user,setUser] = useState({

	id: null,
	isAdmin: null,

})

// useEffect to fetch our user's detail
useEffect(() => {

	fetch('http://localhost:4000/users/getUserDetails',{
		headers:{
			'Authorization': `Bearer ${localStorage.getItem('token')}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		setUser({

			id: data._id,
			isAdmin: data.isAdmin
		})
	})

},[])

console.log(user)

/*
	ReactJS is a single page application (SPA), However, we can actually simulate the changing of the pages. We don't actually create new pages, what we just do is switch pages according to their assigned routes. ReachJS and react-router-dom package just mimic or mirror how HTML accesses it urls.
	
	With this, we don't reload the page each time we switch pages.

	react-router-dom has 3 main components to simulate the changing of pages

	Router
		Wrapping our Router component around our other components will allows us to use routing within our app.
	Switch
		Allows to switch/change our page components
	Route
		Assigns a path which will trigger the change/switch of components to render.
*/
	// Function to clear local storage on logout
	const unsetUser = () => {
		//clears the content of our localStore
		localStorage.clear()
	}


	return(
	<>
		<UserProvider value ={{user,setUser,unsetUser}}>
			<Router>
				<AppNavBar />
				<Container>
					<Switch>
						<Route exact path="/" component ={Home}/>
						<Route exact path="/courses" component ={Courses}/>
						<Route exact path="/courses/:courseId" component ={ViewCourse}/>
						<Route exact path="/login" component ={Login}/>
						<Route exact path="/register" component ={Register}/>
						<Route exact path="/logout" component ={Logout}/>
						<Route exact path="/addCourse" component ={AddCourse}/>
						<Route component ={NotFound}/>
					</Switch>	
				</Container>
			</Router>
		</UserProvider>				
	</>
	)
}