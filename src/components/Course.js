import React,{useState,useEffect} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
export default function Course({courseProp}){
// course document are now being passed from our courses page.
// console.log(courseProp)
/*States in React js are ways to sotre information within a component. The advantage of a state from a variable is that variables do not retain updated information when the component is updated. We destructure the array returned by useState() and assign them in variables. setterFunction can be name however we want, however, by convention, it should instead be named after what state it is updating.
*/
// const [count,setCount] = useState(0);
// const [seats,setSeats] = useState(30);
// const [isActive,setIsActive] = useState(true);
	// useState is react hook which allows us to create a state and its setter function. useState actually return 2 items in an array, with the argument in it becoming the initial value of our state variables. 
	// console.log(useState(0))

	// this console log repeats whenever the enroll button is pressed because whenever a state is updated, the component re-renders.

	// Rendering is react.js, rendering is the act of showing our elements in the component. It is the run of our component. Whenever our component re-renders, it actually runs the function again, the component runs again.

	/*Conditional Rendering is when we are able to show/hide elements based on a condition.*/


	// console.log(courseProp)

/*	everytime the seats state is updated, we will  check if the seat state is equal to zero and if it is, we will change the value of isActive state to false.*/
	// useEffect(() => {

	// 	if(seats === 0){
	// 		setIsActive(false);

	// 	}

	// },[seats]);
	// console.log(isActive)



	// let varCount = 0;
	// console.log(count);
	// console.log(seats);
	// function enroll (){
	// 	// alert("Hello")
	// 	setCount(count + 1);
	// 	setSeats(seats - 1)
	// 	// varCount++;
	// 	// console.log(varCount)
	// }


	return(
	<Card>
		<Card.Body>
			<Card.Title>{courseProp.name}</Card.Title>
			<Card.Subtitle>Description:</Card.Subtitle>
			<Card.Text>{courseProp.description}:</Card.Text>
			<Card.Subtitle>Price:</Card.Subtitle>
			<Card.Text>PHP {courseProp.price}</Card.Text>
			<Link className="btn btn-primary" to={`/courses/${courseProp._id}`}>View Course</Link>
		</Card.Body>
	</Card>
	
		)
}

