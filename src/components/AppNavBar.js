import React,{useContext} from 'react';
import {Navbar,Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../userContext'

export default function AppNavBar() {
	
// userContext hook will allow us to "unwrap"  or use UserContext, get the values passed to it. useCOntext returns an object after unwrapping out context.


// console.log(useContext(UserContext))

	// Destructure the returned object by useCOntext and get our global user state.
	const {user} = useContext(UserContext);
	console.log(user)
	/*
		If you component will have children (such as text-content or other react elements or components), it should have a closing tag. If not, then we stick to our self-closing tag.

		classes is reactjs are added as className instead of class.

		"as" prop allows components to be treated as if they are another component. A component 
	*/


	return (

		<Navbar bg="primary" expand="lg">
			<Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={Link} to="/">Home</Nav.Link>
					<Nav.Link as={Link} to="/courses">Courses</Nav.Link>
					{
					user.id
					? 
							user.isAdmin
							?
							<>
							<Nav.Link as={Link} to="/addCourse">Add Course</Nav.Link>
							<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							</>
							:
							<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							:
							<>
								<Nav.Link as={Link} to="/register">Register</Nav.Link>
								<Nav.Link as={Link} to="/login">Login</Nav.Link>
							</>
					}			
				</Nav>
			</Navbar.Collapse>
		</Navbar>

		)
}