import React from 'react';
import {Row,Col,Button,Jumbotron} from 'react-bootstrap';

// Link component creates an anchor tag, howver, it does not use href and instead it will just commit to switching our page component instead of reloading.
import {Link} from 'react-router-dom';

/*	Row and Col are components from our react-bootstrap module.
  	They create div elements with bootsrap classes.

	react-bootsrap components create react elements with bootsrap classes.

*/

export default function Banner({bannerProp}) {
	
	console.log(bannerProp);

	return (

		<Row>
			<Col>
				<Jumbotron>
				<h1>{bannerProp.title}</h1>
				<p>{bannerProp.description}</p>
				<Link to={bannerProp.destination} className="btn btn-primary">{bannerProp.buttonCallToAction}</Link>
				</Jumbotron>
			</Col>
		</Row>


		)

}