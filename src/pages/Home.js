import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
/*
	Home will be a page component, which will be our pages for our application.

	ReactJS adheres to D.R.Y - Don't Repeat Yourself.


	Props - are data we can pass from a parent component to child component.

	All components actually are able to receive an object, Pros are speical react objects with which we can pass data around from a parent to child.
*/



export default function Home() {

	// can we pass props from Home to Banner?
	let samepleProp = {
			title: "The J-Store Booking System",
			description: "Time to get some studyyyyy",
			buttonCallToAction: "Buy now",
			destination: "/login"
		};
	/*
	
		We can pass  props from a parent to child by adding HTML-like attribute which we can name ourselves. The name of the attribute will become the property name of the objecct received by all components.

		Components are independent from each other. 

	*/
		return (

				<>
					<Banner bannerProp={samepleProp}/>
					<Highlights />
				</>

			)
}