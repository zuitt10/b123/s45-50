import React,{useState,useEffect} from 'react';
// import our mock data
import coursesData from '../data/coursesData';
// import course component
import Course from '../components/Course'


export default function Courses() {

	// create a state with empty array as initial value
	const [coursesArray,setCoursesArray] = useState([]);

// console.log(coursesData)

useEffect(()=>{

	fetch("http://localhost:4000/courses/getActiveCourses")
	.then(res => res.json())
	.then(data =>{
		// resulting new array from mapping the data (which is an array of course documents) will be set into our coursesArray state will its setter function
		setCoursesArray(data.map(course => {
		return (
			// react js when creating a group/array of react elements, we need to pass a key prop so that reactjs can recognized each individual instances of the react element. Each key should be unique.
			
			<Course key={course._id} courseProp={course}/>

			) 

		}))

	})

},[])
console.log(coursesArray);
/*
	
	Each instance of component is  independent from one another.

	So if for example, we called multiple course component, each of those instances are independent from each other. therefore allowing us to have re-usable UI components.

*/


	// let sampleProp1 = "I am sample 1";
	// let sampleProp2 = "I am sample 2";

	// let coursesCards = coursesData.

	// console.log(coursesCards);

	return(
		<>
			<h1 className="my-5">Available Course</h1>
			{coursesArray}
		</>
		)

}