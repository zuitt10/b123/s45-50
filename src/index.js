import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// import bootstrap in index.js, our entry script:
import 'bootstrap/dist/css/bootstrap.min.css'

/*

  Reactjs - syntax used is JSX.

  JSX - Javascript + XML, It is an extension of Javascript that lets us creates objects which will be compiled as a HTML Elements.

  With JSX, we are able to create JS objects with HTML like syntax.
*/

// let element = <h1>My First React App!</h1>
// console.log(element)

// with JSX
// let myName = <h2>Jeric</h2>

// without JSX
// let myName = React.createElement('h2',{},"This was not created with JSX syntax");


// let person = {
//   name: "Stephen Strange",
//   age: 45,
//   job: "Sorcerer Supreme",
//   income: 50000,
//   expense: 30000,
// }

// with JSX
// let sorcererSupreme = 
// <p>
//  My name is {person.name}.
//  I work as {person.job}.
//  My income is {person.income}.
//  My expense is {person.expense}. 
//  My balance is {person.income-person.expense}. </p>


/*ReactJS does not like returning 2 elements unless they are wrapped by another element, or what we call a fragment<></>*/

//  let sorcererSupreme = (

// <>
// <p>
//  My name is {person.name}. I am {person.age}. I work as {person.job}.</p>
//  <p>My name is {person.name}.I work as {person.job}.My income is {person.income}.My expense is {person.expense}. My balance is {person.income-person.expense}. </p>
//  </>

//   )




/*
  Components are functions that return react elements.

  Components naming convention: PascalCase

  Components should start in capital letters.

*/

// const SampleComp = () => {

//   return (
//           <h1>I am returned by a function.</h1>
//     )
// }

// How are components called?
// <SampleComp />
// Create a self-closing tag <sampleComp /> to call your component.

/*

  In React.js, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/main component.

  All other component/pages will be contained in our main component : <App />


  React.StrictMode is a built-in React component which is used to highlight potential problem in the code and in fact allows to provide more information about errors in our code.
*/

ReactDOM.render(<React.StrictMode><App /></React.StrictMode>,document.getElementById('root'));