let coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Non magna irure sunt duis esse laborum laboris dolore occaecat eu tempor in irure fugiat. Magna sint ut.",
		price: 45000,
		onOffer: true
	},

	{
		id: "wdc002",
		name: "Phyton-Django",
		description: "Occaecat non exercitation consequat nisi proident sint proident incididunt quis laborum magna minim dolore aliqua et commodo consequat mollit sunt in et adipisicing enim consectetur nostrud cupidatat nisi laboris anim dolore aliqua.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java Springboot",
		description: "Dolore proident qui incididunt tempor pariatur non fugiat sint deserunt sit enim magna velit cillum in cupidatat labore nostrud do culpa occaecat cillum laboris ut elit velit labore reprehenderit labore ullamco est in sint ea est sint enim aute.",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "Nodejs-MERN",
		description: "Lorem ipsum dolore ut magna aliqua adipisicing nostrud fugiat tempor consectetur laboris proident.",
		price: 45000,
		onOffer: false
	}
]

export default coursesData;