import React,{useState,useEffect} from 'react';
import {Form,Button} from 'react-bootstrap';

export default function Register(){

	/*

		Review

		Props - is a way to pass data from a parent component to a child component. It is used like an HTML attribute added to the child component. The prop then becomes a property of the special react object that all component receive, the props. Prop names are user-defined.

		State - States are a way to store information within a component. This information can then be updated within the component. When a state is updated through its setter function, it will re-render the component.  States are contained within their components. They are independent from other instances of the component.

		Hooks - are Special/react-defined methods and function that allow us to certain task in our components.

		useState() - is a hook that creates states. useState returns an array with 2 items. The first item in the array is state and second one is setter function. We then destructure this returned array and assign both items in variables:

		const[stateName,setterFunction] = useState(initial value of State)

		useEffect() - is a hook that used to create effects. These effects will allow us to run a function or task based on when our effect will run. Our useEffect will ALWAYS run on initial render. The next time that the useEffect will run will depend on its dependency array.

		useEffect(() =>{},[dependency array])
			useEffect will allow us to run a function or task on initial render and whenever our component re-renders IF there is no dependency array.

			useEffect will allow us to run a function or task on initial render ONLY if there is an EMPTY dependecy array

			useEffect will  allow us run a function or task on initial render AND whenever the state/s in its dependency array is updated.

			useEffect will always run on initial render or the very first time our component is displayed or run.
	*/

	const [state1,setState1] = useState(0);
	const [state2,setState2] = useState(0);

	function sample1(){
		setState1(state1 + 1);
	}

	function sample2(){
		setState2(state2 + 1);
	}

	useEffect(() => {

		console.log("I will run only when state 2 is updated.")

	},[state2])

	return (

		<>

			<Button variant="success" onClick={sample1} className="mx-3 my-5">Update State 1</Button>
			<Button variant="danger" onClick={sample2} className="mx-3 my-5">Update State 2</Button>

		</>

		)



		
}