import React from 'react';
import Banner from './Banner';


export default function NotFound(){


	let notFoundProp = {
			title: "Page Not Found",
			description: "Page is not found, please click the button below.",
			buttonCallToAction: "Back to home page",
			destination: "/"
		};
	return (
			<Banner bannerProp={notFoundProp}/>
		)
}